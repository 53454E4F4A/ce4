/*----------------------------------------------------------
    -- Frequenzgenerator
    --
    --@praktikum    CEP
    --@semester     WS13
    --@teamname     S3T4
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann, Jonas     #2045479
    --@aufgabe      Aufgabe A4
    --@kontrolleur  Prof. Dr. Schaefers, Michael
-----------------------------------------------------------*/
 
#include <stdio.h>
#include "lpc24xx.h"
#include "config.h"
#include "ConfigStick.h"
#include "portlcd.h"
#include "fio.h"
 
/* Additional information:
 *
 * PRECISION = 22
 * fSA = 44100 Hz
 * fA1 = 440 Hz
 * fA2 = 5000 Hz
 *
 * Amp: 1.0V, MW: 1.5V
 * Amp: 0.5V, MW: 1.5V
 *
 * LUT1:
 *  sin
 *  fA = 1Hz
 *  NS = 360
 *  Amplitude 1V in Q9.21 = (1V * 1024 * 2^21) / 3.3
 *  DELTA = (NS * fA << PRECISION) / fSA
 *
 * LUT2:
 *  sawtooth
 *  fA = 1Hz
 *  NS = 360
 *  Amplitude 1V in Q9.21 = (1V * 1024 * 2^21) / 3.3
 *  DELTA = (NS * fA << PRECISION) / fSA
 *
 * for fA1:
 *  DELTA = 15065255.183673(..) = ~15065255 (Q9.22)
 * for fA2:
 *  DELTA = 171196081.63265(..) = ~171196081 (Q9.22)
 *
 * Ns = fSA / fA 
 *  for fA1 = 100.2272
 *  for fA2 = 8.82
 *                      
 * Voltage on AOUT = VALUE / 1024 * VREF
 * VREF default is 3.3V.
 *
 *  2.5V * 1024 / VREF = 775.75 -> Maximal benoetigtwer Wert f端r DAC braucht 10 bit -> Tabellenformat Q10.21
 *  Offset/Mittelwert 1.5V in Q10.21
 *  (1.5V * 1024 * 2^21)/3.3
 *  berechnung des wertes fuer dac (OFFSET(MW) + LUT-Wert) >> 21
 *  fuer amp 0.5v (OFFSET + (LUT-Wert >> 1) ) >> 21
 *
 *  defines should be constants as above GCC4.3
 *
 *  48 000 000 / 44 100 = ~1088
 * 
 *  NOTE: As result of startup code, cpu clock (cclk) is already configured to be 48 MHz (UM chapter 4 describes how to)
 *  PCONP  |= (b(1)<<22);       // turn timer2 on - is already done by BaseStickConfig() -> UM Chapter 4-3.4.9, Table 63
 */
 
#define b(n) (                                               \
    (unsigned char)(                                         \
            ( ( (0x##n##ul) & 0x00000001 )  ?  0x01  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00000010 )  ?  0x02  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00000100 )  ?  0x04  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00001000 )  ?  0x08  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00010000 )  ?  0x10  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00100000 )  ?  0x20  :  0 )  \
        |   ( ( (0x##n##ul) & 0x01000000 )  ?  0x40  :  0 )  \
        |   ( ( (0x##n##ul) & 0x10000000 )  ?  0x80  :  0 )  \
    )                                                        \
)
 
#define  S0   ( (GPIO1_IOPIN & (1<< 8))  == 0 )
#define  S1   ( (GPIO1_IOPIN & (1<< 9))  == 0 )
#define  S2   ( (GPIO1_IOPIN & (1<<10))  == 0 )
#define  S3   ( (GPIO1_IOPIN & (1<<11))  == 0 )
#define  S4   ( (GPIO1_IOPIN & (1<<12))  == 0 )
#define  S5   ( (GPIO1_IOPIN & (1<<13))  == 0 )
#define  S6   ( (GPIO1_IOPIN & (1<<14))  == 0 )
#define  S7   ( (GPIO1_IOPIN & (1<<15))  == 0 )
 
#define   FIFO_BUFF_SIZE  ( 16 )
#define   FIFO_INDX_MSK   ( FIFO_BUFF_SIZE - 1 )
 
volatile int fifo[ FIFO_BUFF_SIZE ] = { 0 };     
volatile int fifoRdIndx             =   0;                     
volatile int fifoWrIndx             =   0;                
volatile int fifoUnderflow          =   0;                
 
#define SIG_TAB_NOE 360
#define PRECISION 22
#define WAVE_OFFSET 976128930
#define DELTA440 15059229
#define DELTA5000 171127603
#define MR0_MATCH 1088
#define Amp1V_toVALUE(d)   ((d+WAVE_OFFSET) >> 21)              
#define Amp0p5V_toVALUE(d) (((d>>1)+WAVE_OFFSET) >> 21 )
#define LIMIT              (SIG_TAB_NOE<<PRECISION)              //= 1509949440 Q9.22
#define MAX_INDX5000       (LIMIT-(DELTA5000<<1)+1)              // Q9.22
#define MAX_INDX440        (LIMIT-(DELTA440<<1)+1)               // Q9.22
               
static int signalTableSin[SIG_TAB_NOE] = {
             0,    11357199,    22710939,    34057761,    45394208,    56716828,    68022171,    79306795,    90567260,   101800138,
     113002007,   124169454,   135299078,   146387488,   157431307,   168427172,   179371731,   190261653,   201093619,   211864330,
     222570504,   233208882,   243776222,   254269306,   264684936,   275019941,   285271172,   295435507,   305509849,   315491131,
     325376310,   335162377,   344846350,   354425279,   363896247,   373256369,   382502793,   391632703,   400643318,   409531893,
     418295721,   426932132,   435438495,   443812220,   452050755,   460151590,   468112260,   475930337,   483603442,   491129237,
     498505428,   505729771,   512800062,   519714151,   526469929,   533065339,   539498372,   545767069,   551869520,   557803867,
     563568300,   569161066,   574580459,   579824830,   584892580,   589782167,   594492100,   599020945,   603367323,   607529909,
     611507435,   615298690,   618902520,   622317825,   625543567,   628578762,   631422486,   634073873,   636532114,   638796461,
     640866225,   642740775,   644419540,   645902009,   647187729,   648276310,   649167419,   649860786,   650356199,   650653507,
     650752620,   650653507,   650356199,   649860786,   649167419,   648276310,   647187729,   645902009,   644419540,   642740775,
     640866225,   638796461,   636532114,   634073873,   631422486,   628578762,   625543567,   622317825,   618902520,   615298690,
     611507435,   607529909,   603367323,   599020945,   594492100,   589782167,   584892580,   579824830,   574580459,   569161066,
     563568300,   557803867,   551869520,   545767069,   539498372,   533065339,   526469929,   519714151,   512800062,   505729771,
     498505428,   491129237,   483603442,   475930337,   468112260,   460151590,   452050755,   443812220,   435438495,   426932132,
     418295721,   409531893,   400643318,   391632703,   382502793,   373256369,   363896247,   354425279,   344846350,   335162377,
     325376310,   315491131,   305509849,   295435507,   285271172,   275019941,   264684936,   254269306,   243776222,   233208882,
     222570504,   211864330,   201093619,   190261653,   179371731,   168427172,   157431307,   146387488,   135299078,   124169454,
     113002007,   101800138,    90567260,    79306795,    68022171,    56716828,    45394208,    34057761,    22710939,    11357199,
             0,   -11357199,   -22710939,   -34057761,   -45394208,   -56716828,   -68022171,   -79306795,   -90567260,  -101800138,
    -113002007,  -124169454,  -135299078,  -146387488,  -157431307,  -168427172,  -179371731,  -190261653,  -201093619,  -211864330,
    -222570504,  -233208882,  -243776222,  -254269306,  -264684936,  -275019941,  -285271172,  -295435507,  -305509849,  -315491131,
    -325376310,  -335162377,  -344846350,  -354425279,  -363896247,  -373256369,  -382502793,  -391632703,  -400643318,  -409531893,
    -418295721,  -426932132,  -435438495,  -443812220,  -452050755,  -460151590,  -468112260,  -475930337,  -483603442,  -491129237,
    -498505428,  -505729771,  -512800062,  -519714151,  -526469929,  -533065339,  -539498372,  -545767069,  -551869520,  -557803867,
    -563568300,  -569161066,  -574580459,  -579824830,  -584892580,  -589782167,  -594492100,  -599020945,  -603367323,  -607529909,
    -611507435,  -615298690,  -618902520,  -622317825,  -625543567,  -628578762,  -631422486,  -634073873,  -636532114,  -638796461,
    -640866225,  -642740775,  -644419540,  -645902009,  -647187729,  -648276310,  -649167419,  -649860786,  -650356199,  -650653507,
    -650752620,  -650653507,  -650356199,  -649860786,  -649167419,  -648276310,  -647187729,  -645902009,  -644419540,  -642740775,
    -640866225,  -638796461,  -636532114,  -634073873,  -631422486,  -628578762,  -625543567,  -622317825,  -618902520,  -615298690,
    -611507435,  -607529909,  -603367323,  -599020945,  -594492100,  -589782167,  -584892580,  -579824830,  -574580459,  -569161066,
    -563568300,  -557803867,  -551869520,  -545767069,  -539498372,  -533065339,  -526469929,  -519714151,  -512800062,  -505729771,
    -498505428,  -491129237,  -483603442,  -475930337,  -468112260,  -460151590,  -452050755,  -443812220,  -435438495,  -426932132,
    -418295721,  -409531893,  -400643318,  -391632703,  -382502793,  -373256369,  -363896247,  -354425279,  -344846350,  -335162377,
    -325376310,  -315491131,  -305509849,  -295435507,  -285271172,  -275019941,  -264684936,  -254269306,  -243776222,  -233208882,
    -222570504,  -211864330,  -201093619,  -190261653,  -179371731,  -168427172,  -157431307,  -146387488,  -135299078,  -124169454,
    -113002007,  -101800138,   -90567260,   -79306795,   -68022171,   -56716828,   -45394208,   -34057761,   -22710939,   -11357199
};
 
static int signalTableSaw[SIG_TAB_NOE] = {
             0,     7230585,    14461169,    21691754,    28922339,    36152923,    43383508,    50614093,    57844677,    65075262,
      72305847,    79536431,    86767016,    93997601,   101228185,   108458770,   115689355,   122919939,   130150524,   137381109,
     144611693,   151842278,   159072863,   166303447,   173534032,   180764617,   187995201,   195225786,   202456371,   209686955,
     216917540,   224148125,   231378709,   238609294,   245839879,   253070463,   260301048,   267531633,   274762217,   281992802,
     289223387,   296453971,   303684556,   310915141,   318145725,   325376310,   332606895,   339837479,   347068064,   354298649,
     361529233,   368759818,   375990403,   383220987,   390451572,   397682157,   404912741,   412143326,   419373911,   426604495,
     433835080,   441065665,   448296249,   455526834,   462757419,   469988003,   477218588,   484449173,   491679757,   498910342,
     506140927,   513371511,   520602096,   527832681,   535063265,   542293850,   549524435,   556755019,   563985604,   571216189,
     578446773,   585677358,   592907943,   600138527,   607369112,   614599697,   621830281,   629060866,   636291451,   643522035,
     650752620,   643522035,   636291451,   629060866,   621830281,   614599697,   607369112,   600138527,   592907943,   585677358,
     578446773,   571216189,   563985604,   556755019,   549524435,   542293850,   535063265,   527832681,   520602096,   513371511,
     506140927,   498910342,   491679757,   484449173,   477218588,   469988003,   462757419,   455526834,   448296249,   441065665,
     433835080,   426604495,   419373911,   412143326,   404912741,   397682157,   390451572,   383220987,   375990403,   368759818,
     361529233,   354298649,   347068064,   339837479,   332606895,   325376310,   318145725,   310915141,   303684556,   296453971,
     289223387,   281992802,   274762217,   267531633,   260301048,   253070463,   245839879,   238609294,   231378709,   224148125,
     216917540,   209686955,   202456371,   195225786,   187995201,   180764617,   173534032,   166303447,   159072863,   151842278,
     144611693,   137381109,   130150524,   122919939,   115689355,   108458770,   101228185,    93997601,    86767016,    79536431,
      72305847,    65075262,    57844677,    50614093,    43383508,    36152923,    28922339,    21691754,    14461169,     7230585,
             0,    -7230585,   -14461169,   -21691754,   -28922339,   -36152923,   -43383508,   -50614093,   -57844677,   -65075262,
     -72305847,   -79536431,   -86767016,   -93997601,  -101228185,  -108458770,  -115689355,  -122919939,  -130150524,  -137381109,
    -144611693,  -151842278,  -159072863,  -166303447,  -173534032,  -180764617,  -187995201,  -195225786,  -202456371,  -209686955,
    -216917540,  -224148125,  -231378709,  -238609294,  -245839879,  -253070463,  -260301048,  -267531633,  -274762217,  -281992802,
    -289223387,  -296453971,  -303684556,  -310915141,  -318145725,  -325376310,  -332606895,  -339837479,  -347068064,  -354298649,
    -361529233,  -368759818,  -375990403,  -383220987,  -390451572,  -397682157,  -404912741,  -412143326,  -419373911,  -426604495,
    -433835080,  -441065665,  -448296249,  -455526834,  -462757419,  -469988003,  -477218588,  -484449173,  -491679757,  -498910342,
    -506140927,  -513371511,  -520602096,  -527832681,  -535063265,  -542293850,  -549524435,  -556755019,  -563985604,  -571216189,
    -578446773,  -585677358,  -592907943,  -600138527,  -607369112,  -614599697,  -621830281,  -629060866,  -636291451,  -643522035,
    -650752620,  -643522035,  -636291451,  -629060866,  -621830281,  -614599697,  -607369112,  -600138527,  -592907943,  -585677358,
    -578446773,  -571216189,  -563985604,  -556755019,  -549524435,  -542293850,  -535063265,  -527832681,  -520602096,  -513371511,
    -506140927,  -498910342,  -491679757,  -484449173,  -477218588,  -469988003,  -462757419,  -455526834,  -448296249,  -441065665,
    -433835080,  -426604495,  -419373911,  -412143326,  -404912741,  -397682157,  -390451572,  -383220987,  -375990403,  -368759818,
    -361529233,  -354298649,  -347068064,  -339837479,  -332606895,  -325376310,  -318145725,  -310915141,  -303684556,  -296453971,
    -289223387,  -281992802,  -274762217,  -267531633,  -260301048,  -253070463,  -245839879,  -238609294,  -231378709,  -224148125,
    -216917540,  -209686955,  -202456371,  -195225786,  -187995201,  -180764617,  -173534032,  -166303447,  -159072863,  -151842278,
    -144611693,  -137381109,  -130150524,  -122919939,  -115689355,  -108458770,  -101228185,   -93997601,   -86767016,   -79536431,
     -72305847,   -65075262,   -57844677,   -50614093,   -43383508,   -36152923,   -28922339,   -21691754,   -14461169,    -7230585
};
 
__inline void option_select(int *Amp, int *delta, int **pSigTable, int *maxIdx) {
   
static int last_delta = 0;
 
    if ( S0 ) {
      *delta = DELTA440;
      *maxIdx = MAX_INDX440;
    }
    if ( S1 ) {
      *delta = DELTA5000;
      *maxIdx = MAX_INDX5000;
    }
    if ( S2 ) {
      *pSigTable = signalTableSin;
    }
    if ( S3 ) {
      *pSigTable = signalTableSaw;
    }
    if ( S4 ) {
      *Amp = 0;
    }
    if ( S5 ) {
      *Amp = 1;
    }
    if ( S6 ) {
       *delta = last_delta;
    }
    if ( S7 ) {    
        if(*delta != 0) {
            last_delta = *delta;
        }
        *delta = 0;
    }  
}
 
void __attribute__ ((interrupt("IRQ"))) isr_timer(void) {
 
   /*  | Bit | Symbol        | RV
    *  |  1  | MR0 Interrupt | 0
    */
     
    TIMER2_IR = b(1)<<0;                            
     
    if ( fifoRdIndx != fifoWrIndx ){                 
 
      /*
       * [D/A] [C]onverter [R]egister
       *  Bits | Symbol   |
       *  0:5  | RESERVED | (for future, higher-resolution D/A converters)
       *  6:15 | VALUE    | After the selected settling time after this field is written with a new VALUE, the voltage on the AOUT pin is VALUE/1024 * VRE
       *  16   | BIAS     | settling time / current. 1: ST: 1鐃�s, C: 700 鐃�A 0: ST: 2.5 鐃�s, C: 350 鐃�A. 
       */
         
        DACR = fifo[fifoRdIndx] << 6;      
        fifoRdIndx = (fifoRdIndx + 1) & FIFO_INDX_MSK;
   
    }else{
       
        fifoUnderflow = -1;                       
    }
     
    /* Contains the address of the ISR for the currently active
     * interrupt. This register must be written to (with any val)
     * at the end of an ISR, to update the VIC priority hardware.
     * writing to the register at any other time can cause incorrect
     * operation
     */
 
     VICVectAddr = 0;   
}
 
int main( void ){
     
    int fifoNextWrIndx    =  0;      
    int DELTA             =  DELTA440;
    int MAX_INDX          =  MAX_INDX440;            // Q9.22
    int *pSigTable        =  signalTableSin;      
    int sigTabIndx        =  0;                      // Q9.22
    int Amp               =  0;                
 
    BaseStickConfig();
     
    #ifdef LCD_SUPPORT    
        LCD_puts( "A4 for CEWS13" );
    #endif
     
    // configure GPIO Port1 Pin1&Pin0 as output and clear them (Pin1 is unused in this example)
    GPIO1_IODIR = (b(11)<<0);        // set   Bits 1-0 of GPIO Port 1 as output; -> UM Chapter 10-6, Table 158 & lpc24xx.h
    GPIO1_IOCLR = (b(11)<<0);        // clear Bits 1-0 of GPIO Port 1            -> UM Chapter 10-6, Table 158 & lpc24xx.h
     
    /* A pair of bits in a [P]eripheral [Cl]oc[k]
     * [Sel]ection register controls the rate of
     * the clock signal that will be supplied
     * to the corresponding peripheral.
     *
     *   Bit   |  Symbol     | 00     |  01  |   10   |   11              |
     * --------------------------------------------------------------------
     *  ....   |   ..        | ..     |  ..  |   ..   |   ..              |
     * 12 & 13 | PCLK_TIMER2 | CCLK/4 | CCLK | CCLK/2 | CCLK/8 || CLLK /6 |
     *  ....   |   ..        | ..     |  ..  |   ..   |   ..              |
     *
     */
 
    PCLKSEL1    = (PCLKSEL1 & ~(b(11)<<12)) | (b(01)<<12); // CCLK = 48MHz
     
    /* [T]imer [C]ounter R/W*/
    /* [T]imer [C]ontrol [R]egister  R/W
     * Used to control the Timer Counter functions.
     * The Timer Counter can be disabled or reset
     * trough the TCR
     *
     *  Bit |  Symbol  |    0     |    1   | RV
     *  ----------------------------------------
     *   0  |  Enable  | disable  | enable | 0
     *   1  | (s)Reset | no reset | reset  | 0
     * else | RESERVED | no bitflipping
     */
 
    TIMER2_TCR  =  0x02;
     
    /* [P]rescale [C]ounter R/W */
    /* [P]rescale [R]egister
     * 32bit PC is a counter which is incremented
     * to the value stored in PR. When the value
     * in PR is reached, the TC is incremented and
     * PC is cleared.
     */
 
    TIMER2_PR   =  0;
 
    /* [M]atch [R]egister R/W
     * MR can be enabled through the MCR to reset
     * the TC, stop both the TC and PC, and/or
     * generate an interrupt every time MR0 matches
     * the TC
     */
 
    TIMER2_MR0  =  MR0_MATCH - 1;
     
    /* [I]nterrupt [R]egister
     * The IR can be written to clear interrupts.
     * The IR can be read to identify which 8
     * possible interrupt sources are pending
     *
     * If an interrupt is generated then the corresponding
     * bit in the IR will be high. Otherwise, the bit will be low.
     * Writing a logic one to the corresponding IR bit will reset
     * the interrupt. writing zero has no effect.
     * Bit | Symbol | RV |
     * 0-3 | MR0-3  | 0  | Match
     * 4-7 | CR0-3  | 0  | Capture
     */
 
    TIMER2_IR   =  0xff;     
 
    /* [M]atch [C]ontrol [R]egister
     * The MCR is used to control if an interrupt
     * is generated and if the TC is reset when a
     * match occurs.
     *  Bit  | Symbol |         1          |    0    | RV | for i in 0..3
     * ----------------------------------------------------------------
     *  3i   | MRiI   | generate Interrupt | disable | 0  |
     *  3i+1 | MRiR   | reset TC           | disable | 0  |
     *  3i+2 | MRiS   | stop the TC and PC | disable | 0  |
     * 15:12 | RESERVED
     */
 
    TIMER2_MCR  = (b(011)<<0);
 
    /*
     * [Int]errupt [Select] Register
     * Sets the interrupt sources. Each peripheral
     * device may have one or more interrupt lines
     * to the Vectored Interrupt Controller each
     * line may represent more than one interrupt source
     *
     *  BIT | Symbol |  1  |  0  | RV
     *  ..  |   ..   | ..  | ..  |  
     *  26  | TIMER2 | FIQ | IRQ | 0
     *  ..  |   ..   | ..  | ..  |
     */
   
    VICIntSelect      &=  ~(b(1)<<26);       
 
    /*
     * [Vect]or [Priority] Register
     * Vectory Priority Registers (0-31) designates
     * the priority of the corresponding vectored IRQ
     * slot
     * There are 16 priority levels, corresponding to
     * the values 0..15 of which 15 is the lowest prio
     *
     */
 
    VICVectPriority26  =  5;                  
     
    /*
     * [Vect]or [Addr]ess Register (4byte)
     * Vector Address Registers (0-31) hold the addresses
     * of the ISRs for each IRQ slot
     */
 
    VICVectAddr26      =  (int)( isr_timer ); 
 
    /*
     * [Int]errupt [Enable] Register
     * This Register controls which of the 32 interrupt
     * requests and software interrupts are enabled to
     * contribute to FIQ or RIQ
     */
 
    VICIntEnable       =  (b(1)<<26);      
         
    /* A pair of bits in a [P]eripheral [Cl]oc[k]
     * [Sel]ection register controls the rate of
     * the clock signal that will be supplied
     * to the corresponding peripheral.
     *
     *   Bits  |  Symbol     | 00     |  01  |   10   |   11              |
     * --------------------------------------------------------------------
     *  ....   |   ..        | ..     |  ..  |   ..   |   ..              |
     * 22 & 23 |   PCLK_I2S  | CCLK/4 | CCLK | CCLK/2 | CCLK/8 || CLLK /6 |
     *  ....   |   ..        | ..     |  ..  |   ..   |   ..              |
     *
     */
 
    PCLKSEL0 = ( PCLKSEL0 & ~(b(11)<<22)) | (b(01)<<22);  // 48MHz = CCLK 
     
    /* [Pin] Function [Sel]ect Register
     * A pair of bits in the PINSEL(0-1) Registers control
     * the function of the bits. Apart from GPIO function
     * direction is controlled automatically.
     *
     *  Bits   |   Pin  | 00   | 01     |  10  |  11  | RV |
     *   ..    |   ..   | ..   |     .. |  ..  |  ..  | .. |
     * 20 & 21 | P0[26] | GPIO | AD0[3] | AOUT | RXD3 | 00 |
     *   ..    |   ..   | ..   |  ..    |  ..  |  ..  | .. |
     */
     
    PINSEL1  = ( PINSEL1  & ~(b(11)<<20)) | (b(10)<<20);
 
    // END OF CONFIGURATION
 
    sigTabIndx = 0;
 
    do{
      
        fifoWrIndx = fifoNextWrIndx;                         
         
        if(Amp == 0){
            fifo[ fifoWrIndx ] = Amp1V_toVALUE( pSigTable[ sigTabIndx >> PRECISION ] );  
             
        }else{
         
            fifo[ fifoWrIndx ] = Amp0p5V_toVALUE( pSigTable[ sigTabIndx >> PRECISION ] );
        }
 
        if ( sigTabIndx < MAX_INDX ){                  
            sigTabIndx += DELTA;                           
         
        }else{                                             
         
            sigTabIndx = 0;                                  
        }
 
        fifoNextWrIndx = (fifoWrIndx + 1) & FIFO_INDX_MSK;    
         
    }while ( fifoNextWrIndx != fifoRdIndx );                
   
    // END OF BUFFER INITALISATION
     
    TIMER2_TCR = 0x01;
    
    // MAIN LOOP
    while ( 1 ){
         
        if ( S0 ) {
            TIMER2_TCR = 0x02;                                       
 
        }else{
            
            if ( fifoNextWrIndx != fifoRdIndx ){                
             
                fifoWrIndx = fifoNextWrIndx; 
                 
                if( Amp == 0 ){
                    fifo[ fifoWrIndx ] = Amp1V_toVALUE(pSigTable[ sigTabIndx >> PRECISION ]);  
                 
                }else{
                  
                    fifo[ fifoWrIndx ] = Amp0p5V_toVALUE( pSigTable[ sigTabIndx >> PRECISION ] );  
                }
                 
                if ( sigTabIndx < MAX_INDX ){                   
                    sigTabIndx += DELTA;                             
                
                }else{                                                
                
                    sigTabIndx = 0;                                  
                }
                      
                fifoNextWrIndx = (fifoWrIndx + 1) & FIFO_INDX_MSK; 
            }
             
            TIMER2_TCR = 0x01;          
        }      
         
        option_select(&Amp, &DELTA, &pSigTable, &MAX_INDX);
     
        if ( S7 ){
            fifoUnderflow  =  0;                                  
            GPIO1_IOCLR    =  (b(1)<<0);                       
        }
             
        if ( fifoUnderflow ){
            GPIO1_IOSET    =  (b(1)<<0);                         
        }
    }// END OF MAIN LOOP
 
    // disable interrupt for Timer2 IRQ
    VICIntEnClr  =  (b(1)<<26);   
    // GUARD CODE  -  NEVER STOP !!!  -  THERE IS NO REST
    while(1);
}//main
